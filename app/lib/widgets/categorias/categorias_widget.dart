import 'package:app/models/categorias_model.dart';
import 'package:flutter/material.dart';

class ListaCategorias extends StatelessWidget {
  final List<Category> categorias;
  const ListaCategorias(this.categorias);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: this.categorias.length,
        itemBuilder: (BuildContext context, int index) {
          return _BodyCategory(
            category: this.categorias[index],
            index: index,
          );
        });
  }
}

class _BodyCategory extends StatelessWidget {
  final Category category;
  final int index;

  const _BodyCategory({this.category, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: ListTile(
          title: Text(category.name),
          leading: Image.network(category.logo),
          subtitle: Text(
              'Jornada  ' + category.currentRound + '/' + category.totalRounds),
        ));
  }
}
