import 'package:app/services/ui_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NavigatorBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final uiProvider = Provider.of<UiProvider>(context);

    final currentIndex = uiProvider.selectMenuOpt;
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (int i) => uiProvider.selectMenuOpt = i,
      currentIndex: currentIndex,
      selectedItemColor: Colors.green,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.mail), label: 'Partidos'),
        BottomNavigationBarItem(icon: Icon(Icons.mail), label: 'Torneos'),
        BottomNavigationBarItem(icon: Icon(Icons.mail), label: 'Equipos'),
        BottomNavigationBarItem(icon: Icon(Icons.mail), label: 'Jugadores'),
        BottomNavigationBarItem(icon: Icon(Icons.mail), label: 'Noticias'),
      ],
    );
  }
}
