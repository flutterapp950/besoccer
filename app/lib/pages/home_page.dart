import 'package:app/pages/equipos_page.dart';
import 'package:app/pages/jugadores_page.dart';
import 'package:app/pages/noticias_page.dart';
import 'package:app/pages/partidos_page.dart';
import 'package:app/pages/torneos_page.dart';
import 'package:app/services/ui_provider.dart';
import 'package:app/widgets/navigatorbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Container(
        child: Scaffold(
          body: _HomePageBody(),
          bottomNavigationBar: NavigatorBar(),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Estas seguro?'),
            content: new Text('Quieres salir de la Aplicación'),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text("NO"),
              ),
              SizedBox(height: 16),
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(true),
                child: Text("SI"),
              ),
            ],
          ),
        ) ??
        false;
  }
}

class _HomePageBody extends StatelessWidget {
  const _HomePageBody({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final uiProvider = Provider.of<UiProvider>(context);

    final currentIndex = uiProvider.selectMenuOpt;

    switch (currentIndex) {
      case 0:
        return PartidosPage();

      case 1:
        return TorneosPage();

      case 2:
        return EquiposPage();

      case 3:
        return JugadoresPage();

      case 4:
        return NoticiasPage();

      default:
        return PartidosPage();
    }
  }
}
