import 'package:app/widgets/drawer.dart';
import 'package:flutter/material.dart';

class JugadoresPage extends StatefulWidget {
  JugadoresPage({Key key}) : super(key: key);

  @override
  _JugadoresPageState createState() => _JugadoresPageState();
}

class _JugadoresPageState extends State<JugadoresPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Jugadores'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          )
        ],
      ),
      drawer: DrawerPage(),
      body: Center(
        child: Text('Hola desde Jugadores'),
      ),
    );
  }
}
