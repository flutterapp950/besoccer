import 'package:app/pages/home_page.dart';
import 'package:app/pages/partidos_page.dart';
import 'package:app/pages/splash_screen_page.dart';
import 'package:app/services/categorias_service.dart';
import 'package:app/services/equipos_service.dart';
import 'package:app/services/partidos_service.dart';
import 'package:app/services/ui_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

var routes = <String, WidgetBuilder>{
  "splash": (BuildContext context) => SplashScreen(),
  "partidos": (BuildContext context) => PartidosPage(),
  "home": (BuildContext context) => HomePage(),
};

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => new UiProvider(),
        ),
        ChangeNotifierProvider(create: (_) => new PartidoService()),
        ChangeNotifierProvider(create: (_) => new CategoriaService()),
        ChangeNotifierProvider(create: (_) => new EquipoService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Be Soocer',
        theme: ThemeData(
            primaryColor: Colors.green,
            appBarTheme: AppBarTheme(color: Colors.green)),
        initialRoute: 'splash',
        routes: routes,
      ),
    );
  }
}
