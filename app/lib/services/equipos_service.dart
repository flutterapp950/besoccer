import 'package:app/models/equipos_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EquipoService with ChangeNotifier {
  List<Team> equipos = [];
  final _liga = 10;

  EquipoService() {
    this.getToEquipos();
  }

  getToEquipos() async {
    final url =
        'https://apiclient.besoccerapps.com/scripts/api/api.php?key=8321a5065177ae91e0f0b940a084167a&tz=Europe/Madrid&format=json&req=teams&league=$_liga&year=2020';

    final resp = await http.get(url);

    final teamResponse = teamsFromJson(resp.body);

    print(teamResponse);

    this.equipos.addAll(teamResponse.team);
    notifyListeners();
  }
}
