import 'package:app/services/partidos_service.dart';
import 'package:app/widgets/partidos/partidos_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PartidosHoy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loadmasth = Provider.of<PartidoService>(context).partidosHoy;
    final day = loadmasth;
    return Container(
      child: ListaPartidos(day),
    );
  }
}
