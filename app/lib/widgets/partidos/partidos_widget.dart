import 'package:flutter/material.dart';
import 'package:app/models/partidos_model.dart';

class ListaPartidos extends StatelessWidget {
  final List<Match> partidos;
  const ListaPartidos(this.partidos);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: this.partidos.length,
        itemBuilder: (BuildContext context, int index) {
          return _Partido(
            partido: this.partidos[index],
            index: index,
          );
        });
  }
}

class _Partido extends StatelessWidget {
  final Match partido;
  final int index;

  const _Partido({@required this.partido, @required this.index});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Column(
        children: [
          Center(
            child: Text('Jornada  ' + partido.round),
          ),
          Divider(),
          Center(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding:
                      EdgeInsets.only(bottom: 10, top: 10, left: 15, right: 15),
                  child: Text(
                    partido.local,
                  )),
              Container(
                padding: EdgeInsets.all(0.12),
                height: 20,
                child: Padding(
                  padding: const EdgeInsets.all(0.5),
                  child: Image.network(partido.localShield),
                ),
              ),
              Container(
                child: Text(partido.result),
              ),
              Container(
                padding: EdgeInsets.all(0.12),
                height: 20,
                child: Padding(
                  padding: const EdgeInsets.all(0.5),
                  child: Image.network(partido.visitorShield),
                ),
              ),
              Container(
                  padding:
                      EdgeInsets.only(bottom: 10, top: 10, left: 15, right: 15),
                  child: Text(
                    partido.visitor,
                  )),
            ],
          )),
        ],
      ),
    );
  }
}
