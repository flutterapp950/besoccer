import 'package:app/models/categorias_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CategoriaService with ChangeNotifier {
  List<Category> categorias = [];

  CategoriaService() {
    this.getToCategory();
  }

  getToCategory() async {
    final url =
        'https://apiclient.besoccerapps.com/scripts/api/api.php?key=8321a5065177ae91e0f0b940a084167a&tz=Europe/Madrid&format=json&req=categories&filter=all';

    final resp = await http.get(url);

    final catResponse = categoriasFromJson(resp.body);

    print(catResponse);

    this.categorias.addAll(catResponse.category);
    notifyListeners();
  }
}
