import 'package:app/services/partidos_service.dart';
import 'package:app/widgets/partidos/partidos_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PartidosAyer extends StatelessWidget {
  const PartidosAyer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loadmasth = Provider.of<PartidoService>(context).partidosAyer;
    final day = loadmasth;
    return ListaPartidos(day);
  }
}
