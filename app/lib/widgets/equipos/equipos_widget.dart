import 'package:app/models/equipos_model.dart';
import 'package:flutter/material.dart';

class ListaEquipos extends StatelessWidget {
  final List<Team> equipos;

  const ListaEquipos(this.equipos);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: this.equipos.length,
        itemBuilder: (BuildContext context, int index) {
          return _BodyEquipos(teams: this.equipos[index], index: index);
        });
  }
}

class _BodyEquipos extends StatelessWidget {
  final Team teams;
  final int index;

  const _BodyEquipos({this.teams, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: ListTile(
          title: Text(teams.nameShow),
          subtitle: Text(teams.fullName),
          leading: Image.network(teams.shield),
        ),
      ),
    );
  }
}
