import 'package:app/models/partidos_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:date_format/date_format.dart';

class PartidoService with ChangeNotifier {
  List<Match> partidosAyer = [];
  List<Match> partidosHoy = [];
  List<Match> partidosManana = [];

  var _fechaAyer = formatDate(
      DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day - 1),
      [yyyy, '-', mm, '-', dd]);
  var _fechaHoy = formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd]);
  var _fechaManana = formatDate(
      DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day + 1),
      [yyyy, '-', mm, '-', dd]);

  PartidoService() {
    this.getToPartidosAyer();
    this.getToPartidosHoy();
    this.getPartidosManana();
  }

  getToPartidosAyer() async {
    final url =
        'https://apiclient.besoccerapps.com/scripts/api/api.php?key=8321a5065177ae91e0f0b940a084167a&tz=Europe/Madrid&format=json&req=matchsday&date=$_fechaAyer';

    final resp = await http.get(url);

    final mastResponse = partidosFromJson(resp.body);

    this.partidosAyer.addAll(mastResponse.matches);

    notifyListeners();
  }

  getToPartidosHoy() async {
    final url =
        'https://apiclient.besoccerapps.com/scripts/api/api.php?key=8321a5065177ae91e0f0b940a084167a&tz=Europe/Madrid&format=json&req=matchsday&date=$_fechaHoy';

    final resp = await http.get(url);

    final mastResponse = partidosFromJson(resp.body);

    this.partidosHoy.addAll(mastResponse.matches);

    notifyListeners();
  }

  getPartidosManana() async {
    final url =
        'https://apiclient.besoccerapps.com/scripts/api/api.php?key=8321a5065177ae91e0f0b940a084167a&tz=Europe/Madrid&format=json&req=matchsday&date=$_fechaManana';

    final resp = await http.get(url);

    final mastResponse = partidosFromJson(resp.body);

    this.partidosManana.addAll(mastResponse.matches);
    notifyListeners();
  }
}
