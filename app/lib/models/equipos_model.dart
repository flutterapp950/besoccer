// To parse this JSON data, do
//
//     final teams = teamsFromJson(jsonString);

import 'dart:convert';

Teams teamsFromJson(String str) => Teams.fromJson(json.decode(str));

String teamsToJson(Teams data) => json.encode(data.toJson());

class Teams {
  Teams({
    this.team,
  });

  List<Team> team;

  factory Teams.fromJson(Map<String, dynamic> json) => Teams(
        team: List<Team>.from(json["team"].map((x) => Team.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "team": List<dynamic>.from(team.map((x) => x.toJson())),
      };
}

class Team {
  Team({
    this.id,
    this.idComp,
    this.nameShow,
    this.translate,
    this.groupCode,
    this.favorite,
    this.basealias,
    this.fullName,
    this.conference,
    this.shortName,
    this.alerts,
    this.shield,
    this.shieldBig,
  });

  String id;
  String idComp;
  String nameShow;
  String translate;
  String groupCode;
  String favorite;
  String basealias;
  String fullName;
  String conference;
  String shortName;
  String alerts;
  String shield;
  String shieldBig;

  factory Team.fromJson(Map<String, dynamic> json) => Team(
        id: json["id"],
        idComp: json["id_comp"],
        nameShow: json["nameShow"],
        translate: json["translate"],
        groupCode: json["group_code"],
        favorite: json["favorite"],
        basealias: json["basealias"],
        fullName: json["fullName"],
        conference: json["conference"],
        shortName: json["short_name"],
        alerts: json["alerts"],
        shield: json["shield"],
        shieldBig: json["shield_big"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_comp": idComp,
        "nameShow": nameShow,
        "translate": translate,
        "group_code": groupCode,
        "favorite": favorite,
        "basealias": basealias,
        "fullName": fullName,
        "conference": conference,
        "short_name": shortName,
        "alerts": alerts,
        "shield": shield,
        "shield_big": shieldBig,
      };
}
