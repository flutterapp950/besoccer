import 'package:app/services/categorias_service.dart';
import 'package:app/widgets/categorias/categorias_widget.dart';
import 'package:app/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TorneosPage extends StatefulWidget {
  TorneosPage({Key key}) : super(key: key);

  @override
  _TorneosPageState createState() => _TorneosPageState();
}

class _TorneosPageState extends State<TorneosPage> {
  @override
  Widget build(BuildContext context) {
    final loadcategory = Provider.of<CategoriaService>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Torneos'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          )
        ],
      ),
      drawer: DrawerPage(),
      body: Center(
        child: ListaCategorias(loadcategory.categorias),
      ),
    );
  }
}
