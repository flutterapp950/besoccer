// To parse this JSON data, do
//
//     final categorias = categoriasFromJson(jsonString);

import 'dart:convert';

Categorias categoriasFromJson(String str) =>
    Categorias.fromJson(json.decode(str));

String categoriasToJson(Categorias data) => json.encode(data.toJson());

class Categorias {
  Categorias({
    this.category,
  });

  List<Category> category;

  factory Categorias.fromJson(Map<String, dynamic> json) => Categorias(
        category: List<Category>.from(
            json["category"].map((x) => Category.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "category": List<dynamic>.from(category.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    this.id,
    this.leagueId,
    this.order,
    this.year,
    this.cPending,
    this.alias,
    this.name,
    this.country,
    this.continent,
    this.currentRound,
    this.totalGroup,
    this.totalRounds,
    this.startDate,
    this.endDate,
    this.end,
    this.flag,
    this.logo,
    this.phase,
    this.playoff,
    this.groupCode,
    this.legend,
    this.legendDict,
    this.statusMessages,
  });

  String id;
  String leagueId;
  int order;
  String year;
  String cPending;
  String alias;
  String name;
  String country;
  Continent continent;
  String currentRound;
  String totalGroup;
  String totalRounds;
  DateTime startDate;
  DateTime endDate;
  String end;
  String flag;
  String logo;
  Phase phase;
  dynamic playoff;
  dynamic groupCode;
  List<dynamic> legend;
  List<dynamic> legendDict;
  List<String> statusMessages;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        leagueId: json["league_id"],
        order: json["order"],
        year: json["year"],
        cPending: json["c_pending"] == null ? null : json["c_pending"],
        alias: json["alias"],
        name: json["name"],
        country: json["country"],
        continent: continentValues.map[json["continent"]],
        currentRound: json["current_round"],
        totalGroup: json["total_group"],
        totalRounds: json["total_rounds"],
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        end: json["end"],
        flag: json["flag"],
        logo: json["logo"],
        phase: phaseValues.map[json["phase"]],
        playoff: json["playoff"],
        groupCode: json["group_code"],
        legend: List<dynamic>.from(json["legend"].map((x) => x)),
        legendDict: List<dynamic>.from(json["legend_dict"].map((x) => x)),
        statusMessages: json["status_messages"] == null
            ? null
            : List<String>.from(json["status_messages"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "league_id": leagueId,
        "order": order,
        "year": year,
        "c_pending": cPending == null ? null : cPending,
        "alias": alias,
        "name": name,
        "country": country,
        "continent": continentValues.reverse[continent],
        "current_round": currentRound,
        "total_group": totalGroup,
        "total_rounds": totalRounds,
        "start_date": startDate == null
            ? null
            : "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
        "end_date": endDate == null
            ? null
            : "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
        "end": end,
        "flag": flag,
        "logo": logo,
        "phase": phaseValues.reverse[phase],
        "playoff": playoff,
        "group_code": groupCode,
        "legend": List<dynamic>.from(legend.map((x) => x)),
        "legend_dict": List<dynamic>.from(legendDict.map((x) => x)),
        "status_messages": statusMessages == null
            ? null
            : List<dynamic>.from(statusMessages.map((x) => x)),
      };
}

enum Continent { EU, WO, AM, AF, AS }

final continentValues = EnumValues({
  "af": Continent.AF,
  "am": Continent.AM,
  "as": Continent.AS,
  "eu": Continent.EU,
  "wo": Continent.WO
});

enum Phase { EMPTY, PLAYOFFS, LEAGUE }

final phaseValues = EnumValues(
    {"": Phase.EMPTY, "league": Phase.LEAGUE, "playoffs": Phase.PLAYOFFS});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
