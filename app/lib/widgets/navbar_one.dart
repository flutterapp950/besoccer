import 'package:app/constants/theme_one.dart';
import 'package:flutter/material.dart';

class NavbarOne extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final bool searchBar;
  final bool backButton;
  final bool transparent;
  final bool rightOptions;
  final List<String> tags;
  final Function getCurrentPage;
  final bool isOnSearch;
  final TextEditingController searchController;
  final Function searchOnChanged;
  final bool searchAutofocus;
  final bool noShadow;
  final Color bgColor;

  NavbarOne(
      {this.title = "Home",
      this.tags,
      this.transparent = false,
      this.rightOptions = true,
      this.getCurrentPage,
      this.searchController,
      this.isOnSearch = false,
      this.searchOnChanged,
      this.searchAutofocus = false,
      this.backButton = false,
      this.noShadow = false,
      this.bgColor = ThemeOne.white,
      this.searchBar = false});

  final double _prefferedHeight = 60.0;

  @override
  _NavbarOneState createState() => _NavbarOneState();

  @override
  Size get preferredSize => Size.fromHeight(_prefferedHeight);
}

class _NavbarOneState extends State<NavbarOne> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 5.0, right: 5.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  children: [
                    IconButton(
                        icon: Icon(
                            !widget.backButton
                                ? Icons.menu
                                : Icons.arrow_back_ios,
                            color: !widget.transparent
                                ? (widget.bgColor == ThemeOne.white
                                    ? ThemeOne.initial
                                    : ThemeOne.white)
                                : ThemeOne.white,
                            size: 24.0),
                        onPressed: () {
                          if (!widget.backButton)
                            Scaffold.of(context).openDrawer();
                          else
                            Navigator.pop(context);
                        }),
                    Text(widget.title,
                        style: TextStyle(
                            color: !widget.transparent
                                ? (widget.bgColor == ThemeOne.white
                                    ? ThemeOne.initial
                                    : ThemeOne.white)
                                : ThemeOne.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 18.0)),
                  ],
                ),
                if (widget.rightOptions)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: IconButton(
                            icon: Icon(Icons.search,
                                color: !widget.transparent
                                    ? (widget.bgColor == ThemeOne.white
                                        ? ThemeOne.initial
                                        : ThemeOne.white)
                                    : ThemeOne.white,
                                size: 22.0),
                            onPressed: null),
                      ),
                    ],
                  )
              ],
            ),
          ],
        ),
      ),
    ));
  }
}
