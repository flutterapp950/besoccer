import 'package:app/pages/tabs_partidos/partidos_hoy.dart';
import 'package:app/pages/tabs_partidos/partidos_ma%C3%B1ana.dart';
import 'package:app/pages/tabs_partidos/patidos_ayer.dart';
import 'package:app/widgets/drawer.dart';

import 'package:flutter/material.dart';

class PartidosPage extends StatefulWidget {
  @override
  _PartidosPageState createState() => _PartidosPageState();
}

class _PartidosPageState extends State<PartidosPage> {
  final tabs = [
    'AYER',
    'HOY',
    'MAÑANA',
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.calendar_today_sharp),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.star),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.api_outlined),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.calendar_today_outlined),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            )
          ],
          bottom: TabBar(
            //isScrollable: true,
            tabs: [
              for (final tab in tabs) Tab(text: tab),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            for (final tab in tabs)
              Center(
                child: _bodypage(tab),
              ),
          ],
        ),
        drawer: DrawerPage(
          currentPage: "home",
        ),
      ),
    );
  }
}

_bodypage(tab) {
  switch (tab) {
    case 'AYER':
      return PartidosAyer();

    case 'HOY':
      return PartidosHoy();
    case 'MAÑANA':
      return PartidosManana();

    default:
      return PartidosHoy();
  }
}
