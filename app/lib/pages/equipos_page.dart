import 'package:app/services/equipos_service.dart';
import 'package:app/widgets/drawer.dart';
import 'package:app/widgets/equipos/equipos_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EquiposPage extends StatefulWidget {
  EquiposPage({Key key}) : super(key: key);

  @override
  _EquiposPageState createState() => _EquiposPageState();
}

class _EquiposPageState extends State<EquiposPage> {
  @override
  Widget build(BuildContext context) {
    final loadcategory = Provider.of<EquipoService>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Equipos'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          )
        ],
      ),
      drawer: DrawerPage(),
      body: Center(
        child: ListaEquipos(loadcategory.equipos),
      ),
    );
  }
}
