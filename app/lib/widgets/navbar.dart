import 'package:flutter/material.dart';

class Navbar extends StatelessWidget {
  final String title;

  Navbar({
    this.title = "Home",
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      actions: [
        IconButton(
          icon: Icon(Icons.access_alarm_outlined),
          onPressed: () {},
        )
      ],
    );
  }
}
