// To parse this JSON data, do
//
//     final partidos = partidosFromJson(jsonString);

import 'dart:convert';

Partidos partidosFromJson(String str) => Partidos.fromJson(json.decode(str));

String partidosToJson(Partidos data) => json.encode(data.toJson());

class Partidos {
  Partidos({
    this.matches,
    this.summary,
  });

  List<Match> matches;
  Summary summary;

  factory Partidos.fromJson(Map<String, dynamic> json) => Partidos(
        matches:
            List<Match>.from(json["matches"].map((x) => Match.fromJson(x))),
        summary: Summary.fromJson(json["summary"]),
      );

  Map<String, dynamic> toJson() => {
        "matches": List<dynamic>.from(matches.map((x) => x.toJson())),
        "summary": summary.toJson(),
      };
}

class Match {
  Match({
    this.id,
    this.round,
    this.local,
    this.groupCode,
    this.totalGroup,
    this.visitor,
    this.competitionName,
    this.leagueId,
    this.extraName,
    this.conference,
    this.categoryId,
    this.logo,
    this.team1,
    this.team2,
    this.dteam1,
    this.dteam2,
    this.localAbbr,
    this.visitorAbbr,
    this.numc,
    this.year,
    this.playoffs,
    this.coef,
    this.penaltis1,
    this.penaltis2,
    this.noHour,
    this.extraTxt,
    this.cflag,
    this.localShield,
    this.visitorShield,
    this.date,
    this.hour,
    this.minute,
    this.result,
    this.liveMinute,
    this.status,
    this.winner,
    this.channels,
    this.isVideo,
    this.numVideos,
  });

  String id;
  String round;
  String local;
  String groupCode;
  String totalGroup;
  String visitor;
  String competitionName;
  String leagueId;
  String extraName;
  String conference;
  String categoryId;
  String logo;
  String team1;
  String team2;
  String dteam1;
  String dteam2;
  String localAbbr;
  String visitorAbbr;
  String numc;
  String year;
  bool playoffs;
  String coef;
  String penaltis1;
  String penaltis2;
  bool noHour;
  String extraTxt;
  String cflag;
  String localShield;
  String visitorShield;
  String date;
  String hour;
  String minute;
  String result;
  String liveMinute;
  int status;
  dynamic winner;
  List<Channel> channels;
  int isVideo;
  int numVideos;

  factory Match.fromJson(Map<String, dynamic> json) => Match(
        id: json["id"],
        round: json["round"],
        local: json["local"],
        groupCode: json["group_code"],
        totalGroup: json["total_group"],
        visitor: json["visitor"],
        competitionName: json["competition_name"],
        leagueId: json["league_id"],
        extraName: json["extraName"],
        conference: json["conference"],
        categoryId: json["category_id"],
        logo: json["logo"],
        team1: json["team1"],
        team2: json["team2"],
        dteam1: json["dteam1"],
        dteam2: json["dteam2"],
        localAbbr: json["local_abbr"],
        visitorAbbr: json["visitor_abbr"],
        numc: json["numc"],
        year: json["year"],
        playoffs: json["playoffs"],
        coef: json["coef"],
        penaltis1: json["penaltis1"],
        penaltis2: json["penaltis2"],
        noHour: json["no_hour"],
        extraTxt: json["extraTxt"],
        cflag: json["cflag"],
        localShield: json["local_shield"],
        visitorShield: json["visitor_shield"],
        date: json["date"],
        hour: json["hour"],
        minute: json["minute"],
        result: json["result"],
        liveMinute: json["live_minute"],
        status: json["status"],
        winner: json["winner"],
        channels: List<Channel>.from(
            json["channels"].map((x) => Channel.fromJson(x))),
        isVideo: json["isVideo"] == null ? null : json["isVideo"],
        numVideos: json["numVideos"] == null ? null : json["numVideos"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "round": round,
        "local": local,
        "group_code": groupCode,
        "total_group": totalGroup,
        "visitor": visitor,
        "competition_name": competitionName,
        "league_id": leagueId,
        "extraName": extraName,
        "conference": conference,
        "category_id": categoryId,
        "logo": logo,
        "team1": team1,
        "team2": team2,
        "dteam1": dteam1,
        "dteam2": dteam2,
        "local_abbr": localAbbr,
        "visitor_abbr": visitorAbbr,
        "numc": numc,
        "year": year,
        "playoffs": playoffs,
        "coef": coef,
        "penaltis1": penaltis1,
        "penaltis2": penaltis2,
        "no_hour": noHour,
        "extraTxt": extraTxt,
        "cflag": cflag,
        "local_shield": localShield,
        "visitor_shield": visitorShield,
        "date": date,
        "hour": hour,
        "minute": minute,
        "result": result,
        "live_minute": liveMinute,
        "status": status,
        "winner": winner,
        "channels": List<dynamic>.from(channels.map((x) => x.toJson())),
        "isVideo": isVideo == null ? null : isVideo,
        "numVideos": numVideos == null ? null : numVideos,
      };
}

class Channel {
  Channel({
    this.id,
    this.name,
    this.image,
  });

  String id;
  String name;
  String image;

  factory Channel.fromJson(Map<String, dynamic> json) => Channel(
        id: json["id"],
        name: json["name"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
      };
}

class Summary {
  Summary({
    this.today,
    this.spain,
    this.live,
  });

  String today;
  String spain;
  String live;

  factory Summary.fromJson(Map<String, dynamic> json) => Summary(
        today: json["today"],
        spain: json["spain"],
        live: json["live"],
      );

  Map<String, dynamic> toJson() => {
        "today": today,
        "spain": spain,
        "live": live,
      };
}
